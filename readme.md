# 1.1.1 20180326
配合ewt-econn升级

# 1.1.0 20180208
配合ewt-httpio v1.1.0版本，ajax请求需要新方式

# 1.0.4 20171219
修复ajax方法中，try{} 中不能含有doResponse方法

# 1.0.3 20171205
修复task方法遇到0个任务时，无法触发callback问题
修复classBase启动失败时，状态没有回归stop的问题;
修复funcParamsOrder遇到undefined时，后续参数匹配失败问题。