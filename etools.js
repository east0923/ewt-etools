'use strict';
const URL=require('url');
const net=require('net');
const crypto=require('crypto');
const os=require('os');
const fs=require('fs');
const clone=require('clone');

const conf={
  ajaxReq:{
    resMaxLength            : 2*1024*1024, // ajax请求返回最大长度(字节)
    resAlowNoContentLength  : true,        // 是否允许反馈请求不带'Content-Length'信息
  }
};

const etools={};

/* ============== javascript语法拓展 ============== */
Array.prototype.removeElement=function(item){
  let n=0;// 已经移除的个数
  // 相等项目挤掉
  for(let i=0;i<this.length;i++){
    if(this[i]===item||
      (Number.isNaN(this[i])&&Number.isNaN(item))
    ){
      // 相等的需要移除，只对计数器+1
      n++
    } else {
      // 不想等，向前移位
      this[i-n]=this[i];
    }
  }
  // 从后删除n个元素
  for(let i=0;i<n;i++) this.pop()
};
// 克隆对象
etools.clone=clone;// 引用第三方库

// 将json序列化为二进制流
etools.json2buf=(obj)=>{
  const bufArry=[];
  switch (etools.protoType(obj)){
    case '[object Array]':
      bufArry.push(Buffer.from('['));
      for(let i=0;i<obj.length;i++){
        if(i>0) bufArry.push(Buffer.from(','));
        bufArry.push(etools.json2buf(obj[i]));
      }
      bufArry.push(Buffer.from(']'));
      return Buffer.concat(bufArry);
    case '[object Object]':
      const keyArry=Object.keys(obj).sort();
      bufArry.push(Buffer.from('{'));
      for(let i=0;i<keyArry.length;i++){
        if(i>0) bufArry.push(Buffer.from(','));
        const key=keyArry[i];
        bufArry.push(Buffer.from(`"${key}":`));
        bufArry.push(etools.json2buf(obj[key]));
      }
      bufArry.push(Buffer.from('}'));
      return Buffer.concat(bufArry);
    case '[object Uint8Array]':// 即buffer
      return obj;
    case '[object Number]':
      return Buffer.from(obj.toString());
    case '[object String]':
      return Buffer.from(`"${obj}":`);
    case '[object Null]':
      return Buffer.from('null');
    case '[object Undefined]':
      return Buffer.from('undefined');
    case '[object Boolean]':
      return Buffer.from(obj.toString());
    default:
      throw new Error('unknow type: '+Object.prototype.toString.call(obj))
  }
};

// 获取json的md5特征key
etools.jsonMd5Key=(json)=>{
  if(etools.protoType(json)!=='[object Object]') throw new Error('json must be [object Object]');
  return etools.md5(etools.json2buf(json),undefined,'hex');
};

// 同步写文件(含创建文件夹过程)
etools.writeFile=(filepath,buf)=>{
  if(filepath.indexOf(':')>=0) throw new Error('cannot incloude ":"');
  if(filepath.indexOf('\\')>=0) throw new Error('cannot incloude "\\"');

  // 如果是相对路径，则变绝对路径
  if(filepath.substr(0,1)!=='/') filepath=process.cwd()+'/'+filepath;

  const strArry=filepath.split('/').slice(1);

  // 循环验证各级目录是否存在，不存在则建立
  let dirPath='';
  for(let i=0;i<strArry.length-1;i++){
    dirPath+='/'+strArry[i];
    if(!fs.existsSync(dirPath))
      fs.mkdirSync(dirPath)
  }
  // 写文件
  fs.writeFileSync(filepath,buf)
};

// 动态从指定位置载入json文件
etools.syncJson=(obj,path,onChangeFunc)=>{
  // 清空obj对象
  const objClear=(obj)=>{
    const keys=Object.keys(obj);
    for(const key of keys){
      delete obj[key];
    }
  };

  // (异常) 输入检验
  if(!etools.typeMatch(obj,'object')) throw new Error('obj must be object');
  if(!etools.typeMatch(path,'string')) throw new Error('path must be string');
  /* ====== (异常)首次同步载入 ====== */
  // 读文件
  const utf8=fs.readFileSync(path,'utf8');
  // 转json
  const json=JSON.parse(utf8);
  // 清空原对象
  objClear(obj);
  // 遍历赋值
  for(const key in json){
    obj[key]=json[key];
  }
  // 如果有onChangeFunc,则执行
  if(typeof onChangeFunc==='function') onChangeFunc();

  // 文件监控
  const watcher=fs.watchFile(path,(curr,prev)=>{
    // 只有修改时间不同时，重新载入
    if(curr.mtime===prev.mtime)return;

    // 尝试重新载入
    try{
      // 读文件
      const utf8=fs.readFileSync(path,'utf8');
      // 转json
      const json=JSON.parse(utf8);
      // 清空原对象
      objClear(obj);
      // 遍历赋值
      for(const key in json){
        obj[key]=json[key];
      }
      // 如果有onChangeFunc,则执行
      if(typeof onChangeFunc==='function') onChangeFunc();
    } catch(e) {
      etools.log(`reLoad dyncJson(${path}) Error: `+e)
    }
  });
};
etools.unSyncJson=(path)=>{
  fs.unwatchFile(path);
};

// 写带时间前缀的日志
etools.log=(str)=>{
  console.log(etools.time.nowLocalStr()+': '+str)
};

// 错误日志统一格式
etools.errlog=(title,obj)=>{
  // 输出title
  title='==== '+title+' ====';
  etools.log(title);
  // 输出obj
  switch (typeof obj){
    case 'string':
      etools.log(obj);
      break;
    case 'obj':
      try{
        etools.log(JSON.stringify(obj));
      } catch (e) {
        etools.log(obj.toString());
      }
      break;
    default:
      // 不知道格式，不输出Obj，不输出^行
      return;
  }
  // 输出结束行
  let str='';
  for(let i=0;i<title.length;i++) str+='^';
  etools.log(str);
};

// 并行任务统一等待结果
etools.task=function(timeoutSecond){return new c_etask(timeoutSecond)};
class c_etask{
  constructor(timeoutSecond){
    this.out_count=0;
    this.rout=[]
    // 如果有超时设定，启动超时定时器
    if(timeoutSecond>0)
      setTimeout(()=>{
        if(typeof this.final==='function'){
          this.final(this.rout)
        }
        delete this.final;
      },timeoutSecond*1000)
    // 如果任务量为0，则立即尝试执行final
    process.nextTick(()=>{
      if(this.rout.length===0){
        if(typeof this.final==='function'){
          this.final(this.rout)
        }
        delete this.final;
      }
    })
  }

  do(func,...other){
    // ===== 每次调用 =====

    let count=this.rout.length;  // 记录当前调用序号
    this.rout.push(['TIMEOUT']); // 结果数组增长一位

    // 闭包内定义回调函数
    const callback=(...all)=>{
      // 将反馈结果记录入结果数组
      this.rout[count]=all;
      // 有结果反馈计数+1
      this.out_count++;
      // 当有结果计数与调用次数相同时，执行类的反馈
      if(this.out_count===this.rout.length){
        // 调用final
        if(typeof this.final==='function'){
          this.final(this.rout)
        }
        delete this.final;
      }
    };

    // 强制异步执行
    process.nextTick(()=>{
      other.push(callback);
      func.apply(func,other)
    })
  }
}

// 实例基类
/* 使用说明：
 * 继承此类，需实现以下接口：
 * 内部方法：
 * _boot(callback) ---启动方法，会在start中调用
 * _stop(callback) ---停止方法，会在stop中调用
 * _destroy()      ---销毁方法(可选)
 *
  * code:
  * 0 正常
  * [1~99] 为基类自己创建
  * 1~39   warn
  *  1: 非停止状态调用start方法
  *  2: 非工作状态调用stop 方法
  * 40~79   error
  *  40: 启动错误
  *  41: 停止错误
  * 80~99   close
  *  80: 销毁时遇到错误
  * */
etools.classBase=class extends require('events'){
  constructor(conf_default){
    // 构造函数参数接收全量、默认配置项目
    super();
    if(!etools.typeMatch(conf_default,'object')) throw new Error('Build classbase Error');
    this._conf=conf_default;

    // 是否激发过 firstStart事件
    this._hasFirstStart=false;

    // 最后一次的错误
    this._last_err=null;
    this._last_code=0;

    // 消息输出头，为空则不输出
    this._showMsgHead=undefined;

    // 秒周期函数激发器
    this._secondInterval=setInterval(()=>{
      if(typeof this._secondFunc==='function') this._secondFunc();
    },1000);

    // 状态
    /*
    * stop   : 停止状态
    * booting: 启动中
    * work   : 正常运行
    * close  : 不可再用
    * */
    this._status='stop';

  }
  // 更新配置文件
  _renewConf(conf){
    if(!etools.typeMatch(conf,'object')) throw new Error('Renew class Conf Error');
    // 默认配置文件中有的项目，才可以更新
    for(const key in conf){
      if(key in this._conf){
        this._conf[key]=conf[key];
      }
    }
  }
  // 触发通用事件
  _emitMsg(msg)       {
    // 是否需要输出日志
    if(this._showMsgHead){
      etools.log(`[${this._showMsgHead} msg] `+msg)
    }
    this.emit('msg'  ,msg)}
  _emitWarn(code,msg) {
    // 是否需要输出日志
    if(this._showMsgHead){
      etools.log(`[${this._showMsgHead} warn-${code}] `+msg)
    }
    this.emit('warn' ,code,msg)}
  _emitStop(code,err) {
    // 是否需要输出日志
    if(this._showMsgHead){
      if(etools.typeMatch(err,['object','array'])) try{err=JSON.stringify(err)}catch (e){}
      etools.log(`[${this._showMsgHead} stop-${code}] `+err)
    }

    this._status='stop';
    this._last_err=err;
    this._last_code=code;
    this.emit('stop',code,err);
  }
  _emitClose(code,err){
    // 是否需要输出日志
    if(this._showMsgHead){
      if(etools.typeMatch(err,['object','array'])) try{err=JSON.stringify(err)}catch (e){}
      etools.log(`[${this._showMsgHead} close-${code}] `+err)
    }

    this._status='close';
    this._last_err=err;
    this._last_code=code;
    this.emit('close',code,err);
  }

  /* 对外可写属性 */
  // 设置msg、warn、stop、close事件输出日志的消息头，若为空，则不输出日志
  set eventLogHead(head){
    if(!head||(typeof head!=='string'))
      this._showMsgHead=undefined;
    else
      this._showMsgHead=head;
  }

  // 对外只读属性
  get status() { return this._status }
  get lastErr(){
    return {
      code:this._last_code,
      err:this._last_err
    }
  }

  /*========= 对外方法 =========*/

  // 启动实例，必须在stop状态调用
  start(){
    // 非停止状态使用
    if(this.status!=='stop'){
      // 触发警告
      this._emitWarn(1,'do start, should at STOP status');
    }
    // 正常调用，停止状态
    else {
      // 标记为启动中状态
      this._status='booting';
      this._boot((err)=>{
        // 启动失败，触发error事件
        if(err){
          this._status='stop';
          this._stop(()=>{});
          this._emitStop(40,err);
        }
        // 启动成功
        else {
          this._status='work';
          // 判断是否是第一次启动
          if(!this._hasFirstStart){
            // 触发firstStart事件
            this.emit('firstStart');
            this._hasFirstStart=true;
          }
          // 触发start事件
          this.emit('start');
        }
      })
    }
  }
  // 停止实例，必须在work状态调用
  stop(){
    // 非停止状态使用
    if(this.status!=='work'){
      // 触发警告
      this._emitWarn(2,'do stop, should at WORK status');
    }
    // 正常调用，停止状态
    else {
      // 标记为启动中状态
      this._status='stop';
      this._stop((err)=>{
        // 停止失败，触发error事件
        if(err){
          this._emitStop(41,err)
        }
        // 停止成功
        else {
          this._emitStop();
        }
      })
    }
  }
  // 销毁实例
  destroy(){
    this._status='close';
    // 依次调用自定义的关闭、销毁方法
    this._stop((err)=>{
      if(err){
        this._emitClose(80,err);
      } else {
        this._emitClose();
      }
      // 实例的销毁方法
      if(typeof this._destroy==='function') this._destroy();
      // 基类自己的销毁方法
      this.removeAllListeners(this.eventNames());
    });
    // 销毁秒周期调用
    clearInterval(this._secondInterval);
  }
};

// 判断a是否等于b，支持多种类型
etools.equals=function selfFunc(a,b,count=0){
  count++;
  if(count>10) return false; // 避免回调死循环

  switch (typeof a){
    case 'number':
    case 'boolean':
    case 'undefined':
    case 'string': return a===b;
    case 'object':
      if(typeof b!=='object') return false;
      // 都是数组 或 都是流
      if(Array.isArray(a)&&Array.isArray(b)||Buffer.isBuffer(a)&&Buffer.isBuffer(b)){
        if(a.length!==b.length) return false;
        for(let i=0;i<a.length;i++){
          // 回调自身，不相等则反馈
          if(!selfFunc(a[i],b[i],count)) return false
        }
        return true
      }
      // 还有数组或流
      else if(Array.isArray(a)||Array.isArray(b)||Buffer.isBuffer(a)||Buffer.isBuffer(b)){
        return false
      }
      // 是否是null
      else if(a===null){
        return b===null
      }
      // 此处认为object是json对象
      else {
        // key 数量不同，则反馈false
        if(Object.keys(a).length!==Object.keys(b).length) return false;
        for(const key in a){
          // a中有b中没有的项目，反馈false
          if(!(key in b)) return false;
          // ab中同一个键下有不同的值，反馈false
          if(!selfFunc(a[key],b[key],count)) return false
        }
        return true
      }
    default:
      // 未识别的类别
      return false
  }
};

// 函数参数按照etypeArry顺序整理
etools.funcParamsOrder=function(other,etypeArry){
  // 整理后的数组
  const newOther=[];
  let j=0; // other中查找的指针
  for(let i=0;i<etypeArry.length;i++){
    // 匹配成功
    if(etools.typeMatch(other[j],etypeArry[i])){
      // 添加到数组
      newOther.push(other[j]);
      // other指针到下一个
      j++;
    }
    // 匹配失败
    else {
      // 数组中添加undefined，other指针不动
      newOther.push(undefined);
      // 如果other中该项是undefined，则指针到下一个
      if(etools.typeMatch(other[j],'undefined')) j++;
    }
  }
  return newOther;
};

/* ============== 时间相关 ============== */
etools.time={
  // 输出当前标准时间
  nowLocalStr:function () {
    return etools.time.stamp2fmt(new Date())
  },
  // 输出指定时间戳对应的字符串
  stamp2fmt:(stamp,fmt)=>{
    if(typeof stamp==='number')stamp=new Date(stamp);
    if(!fmt)fmt='yyyy-MM-dd hh:mm:ss';
    const o = {
      "M+": stamp.getMonth() + 1, //月份
      "d+": stamp.getDate(), //日
      "h+": stamp.getHours(), //小时
      "m+": stamp.getMinutes(), //分
      "s+": stamp.getSeconds(), //秒
      "q+": Math.floor((stamp.getMonth() + 3) / 3), //季度
      "S": stamp.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (stamp.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (const k in o)
      if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt
  },
  // 返回指定年份2月的天数
  feburaryDays:function(fullyear){
    if(!etools.isInt(fullyear)) throw new Error('fullyear isnot INT')
    if(fullyear%100===0){
      return (fullyear%400===0)?29:28
    } else {
      return (fullyear%4===0)?29:28
    }
  },
  // 输出日期对象所在的年，周数
  weekNum:function _weekNum(Year,Month,Date,firstDay){
    if(firstDay===undefined)firstDay=0;
    // 输入检查
    if( !etools.isInt(Year)||
      !etools.isInt(Month)||Month<1||Month>12||
      !etools.isInt(Date) ||Date<1 ||Date>31||
      !(firstDay!==0&&firstDay!==1))
      throw new Error('input Year/Month/Date/firstDay Error');

    // 将Date计算为当年的第几天
    switch (Month-1){
      case 11: Date+=30;
      case 10: Date+=31;
      case  9: Date+=30;
      case  8: Date+=31;
      case  7: Date+=31;
      case  6: Date+=30;
      case  5: Date+=31;
      case  4: Date+=30;
      case  3: Date+=31;
      case  2: Date+=etools.time.feburaryDays(Year);
      case  1: Date+=31;
    }

    const passnum=Math.floor(Date/7); // 已过去的整周数
    const leftday=Date%7; //除去整周的余数
    const day    =(new Date(Year,1,1)).getDay(); // 当年1月1日的星期数

    const ld=(7-day+firstDay)%7; // 上年剩余天数

    // 根据剩余天数和上年剩余天数反馈
    if(leftday>ld) return [Year,passnum+1];
    else if(passnum>0) {
      return [Year,passnum]
    } else {
      return _weekNum(Year-1,12,31)
    }
  }
};

/* ============== Buffer流处理 ============== */

// 获取raw中，0所在的位置，反馈数组
etools.bufFind0=function(raw,start,maxCount){
  if(!Buffer.isBuffer(raw)) throw new Error("input raw isNot Buffer");
  if(!etools.isInt(start)||start<0) start=0;          // 默认从0位开始
  if(!etools.isInt(maxCount)||maxCount<1)maxCount=1;  // 默认检出1个
  const rArry=[];
  for(let i=start;i<raw.length;i++){
    if(raw[i]===0){
      rArry.push(i);
      if(rArry.length>=maxCount) return rArry;
    }
  }
  return rArry;
};
// 将一个json对象和一个raw对象合并成一个Buffer
etools.jr2buffer=function(json,raw){
  // 格式验证
  if(json&&!etools.typeMatch(json,['object','array'])) throw new Error('json must be Object');
  else{
    // 将json转化为流
    json=Buffer.from(JSON.stringify(json),'utf8');
  }
  if(raw &&!etools.typeMatch(json,'buffer')) throw new Error("input raw isNot Buffer");

  // 流整理
  let mixJsonRaw;
  const zeroBuffer=Buffer.alloc(1);
  // 有 json 和 raw
  if(json && raw){
    mixJsonRaw=Buffer.concat([json,zeroBuffer,raw])
  }
  // 有json
  else if(json){
    mixJsonRaw=Buffer.concat([json])
  }
  // 有raw
  else if(raw){
    mixJsonRaw=Buffer.concat([zeroBuffer,raw])
  }
  // 没有json也没有raw
  else {
    mixJsonRaw=Buffer.allocUnsafe(0)
  }
  return mixJsonRaw
};
// 将一个Buffer拆分成一个json对象和一个raw对象
etools.buffer2jr=function(buffer){
  if(!etools.typeMatch(buffer,'buffer')) throw new Error("input raw isNot Buffer");

  let json,raw;
  // 解析过程
  const arry0=etools.bufFind0(buffer);

  if(buffer.length===0){
    // 不用做任何处理
  } else if(arry0.length===0){
    // 没有0位分割，全是json
    json=buffer
  } else if(arry0[0]===0){
    // 0位分割在0位，表示无json，只有raw
    raw=buffer.slice(1)
  } else {
    // 有0位分割，且前后均有数据
    json=(buffer.slice(0,arry0[0]));
    raw=buffer.slice(arry0[0]+1)
  }
  // json实例化
  if(json){
    json=json.toString('utf8');
    try{
      json=JSON.parse(json);
    } catch (e){
      json=null;
    }
  } else {
    json=null;
  }

  return [json,raw]
};


/* ============== 网络通讯辅助 ============== */

// 由http请求头中的cookie字符串提取cookie的JsonObject
etools.getCookie=(cookieStr)=>{
  if(!etools.typeMatch(cookieStr,'string')) return {};
  // 有cookie项目，分离
  const rObj={};
  cookieStr.split(';').forEach((item)=>{
    const [key,value]=item.split('=').map((i)=>{return i.trim()});
    // 如果value
    rObj[key]=value
  });
  return rObj
};

// 各种mime类型向json的转换，以及其反向转换
const mime2jr_catch=[
  {
    mime:'application/json',
    func:(raw)=>{
      return [JSON.parse(raw.toString()),undefined]
    }
  },
  {
    mime:'application/x-www-form-urlencoded',
    func:(raw)=>{
      const json={}
      let str=raw.toString();
      // formData格式
      if(str.substr(0,4)==='----'){
        throw new Error('do not support: form-data')
        str=str.replace(/\r\n/g,'\r')
        const lineArry=str.split('\r')
      }
      // 普通格式
      else {
        // 转字符串，按照&切开
        const itemArry=str.split('&');
        itemArry.forEach((item)=>{
          // 每个元素取出赋值给json
          const [key,value]=item.split('=').map((i)=>{return i.trim()})
          json[key]=value
        })
      }
      return [json,undefined]
    }
  }
]
etools.mime2jr_catch=(mime,raw)=>{
  // mime匹配
  for(let i=0;i<mime2jr_catch.length;i++){
    const item=mime2jr_catch[i]
    if(mime.indexOf(item.mime)>=0) return item.func(raw)
  }
  // 没有匹配
  throw new Error('no match MIME')
}
// 各种json向mime类型的转换
const jr2mime_catch=[
  {
    mime:'application/json',
    func:(json)=>{
      // 换算输出的流
      const buf=Buffer.from(JSON.stringify(json));
      // 输出 0：流，1：需要添加的headers
      return [
        buf,
        {
          'Content-Type':'application/json',
          'Cache-Control':'no-cache',
          'Content-Length':buf.length
        }
      ]
    }
  },
  {
    mime:'application/x-www-form-urlencoded',
    func:(json)=>{
      // 生成buf流
      const itemArry=[];
      for(const item in json){
        itemArry.push(`${item}=${json[item]}`)
      }
      const buf=Buffer.from(itemArry.join('&'));
      // 输出 0：流，1：需要添加的headers
      return [
        buf,
        {
          'Content-Type':'application/x-www-form-urlencoded',
          'Cache-Control':'no-cache',
          'Content-Length':buf.length
        }
      ]
    }
  },
  {
    mime:'multipart/form-data',
    func:(json)=>{
      // 生成随机分隔符
      const boundary='------LSD'+etools.ranStr(23);
      // 生成buf流
      const BufArry=[];
      for(const item in json){
        BufArry.push(Buffer.from(`--${boundary}\r\nContent-Disposition: form-data; name="${item}"\r\n\r\n${json[item]}\r\n`))
      }
      BufArry.push(Buffer.from('--'+boundary+'--'));
      const buf=Buffer.concat(BufArry);

      // 输出 0：流，1：需要添加的headers
      return [
        buf,
        {
          'Content-Type':'multipart/form-data; boundary='+boundary,
          'Cache-Control':'no-cache',
          'Content-Length':buf.length
        }
      ]
    }
  }
]
etools.jr2mime_catch=(mime,json,raw)=>{
  // 移除mime中的*号
  mime=mime.replace(/\*/g,'')
  // mime匹配
  for(let i=0;i<jr2mime_catch.length;i++){
    const item=jr2mime_catch[i]
    // 双向任意一方包含对方都行
    if(mime.indexOf(item.mime)>=0 || item.mime.indexOf(mime)>=0){
      // 将识别到的mime添加到最前面
      const result=item.func(json,raw);
      result.unshift(item.mime);
      // 反馈结果
      return result
    }
  }
  // 没有匹配，且mime不是'/'，按照'/' 迭代调用一次，以匹配默认项
  if(mime!=='/') return etools.jr2mime_catch('/',json,raw);
  // mime已经是'/'，没办法了，报错
  else throw new Error('no match MIME')
}


// 生成tcpClient的ID，输入ip和port
etools.ip2raw=function selfFunc(ip){
  // 当传入已经是二进制流时，进行格式验证
  if(Buffer.isBuffer(ip)){
    // 验证长度是16位
    if(ip.length===16) return ip;
    // 长度不是16位，报错
    throw new Error('ip is Buffer but length isnot 16: '+ip);
  }
  // 至此IP不是字符串时，报错
  else if(typeof ip!=='string') throw new Error('ip is not string/buffer, type: '+(typeof ip));

  // 按照字符串识别IP地址
  ip=ip.toLowerCase();
  switch (net.isIP(ip)){
    case 6:
      var raw=Buffer.allocUnsafe(16);
      var arry=ip.split(':');
      var v4=ip.indexOf('.')>=0?1:0;
      var jump=8-arry.length-v4;
      var p=0;
      for(var i=0;i<arry.length;i++){
        var str=arry[i];
        switch (str){
          case "":
            if(i>0){
              raw.fill(0,p,p+jump*2);
              p+=jump*2
            }
          case "0":
            raw.fill(0,p,p+2);
            p+=2;
            break;
          case "1":
            raw.fill(255,p,p+2)
            p+=2
            break
          default:
            var ary2=str.split('.')
            if(ary2.length>1){
              // ipv4整体
              for(var j=0;j<4;j++){
                raw.fill(Number.parseInt(ary2[j]),p,p+1);
                p++
              }
            } else {
              // ipv6一段
              var buf=Buffer.from(str,'hex');
              for(var j=0;j<buf.length;j++){
                raw[p+j]=buf[j]
              }
              p+=buf.length
            }
        }
      }
      return raw;
      break;
    case 4:
      return selfFunc('::ffff:'+ip);
    default:
      throw new Error('input is NOT ip')
  }
};
// 判定两个ip地址是否相等，支持ipv4与v6，转换后相等也为真
etools.ipEqu=(ip1,ip2)=>{
  // 将两个ip转化为二进制流
  var raw1=etools.ip2raw(ip1)
  var raw2=etools.ip2raw(ip2)
  // 按位比较，遇不等则返回false
  for(var i=0;i<raw1.length;i++){
    if(raw1[i]!=raw2[i]) return false
  }
  // 全部相等，返回true
  return true
};
// 将ip地址、端口转化为唯一性id
etools.sockId=function(ip,port){
  return Buffer.concat([etools.ip2raw(ip),Buffer.from([port&255,port>>8])]).toString('hex');
};
// 字符串是域名、Ipv4地址，Ipv6地址
etools.isDomain=function(str){
  var regex= "([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}"
  var i= _strTestRegex(str,regex)
  return i
};
etools.isIpv4=function(str){
  return net.isIPv4(str)
};
etools.isIpv6=function(str){
  return net.isIPv6(str)
};

// 字符串是URL
etools.isUrl=function(str){
  // 非字符串报错
  if(typeof str!=='string') return false;
  // 生成urlObj对象
  let urlObj;
  try{
    urlObj=URL.parse(str)
  } catch (e) {
    return false
  }
  // 协议部分验证 protocol
  if(!urlObj.protocol || 'http: https:'.indexOf(urlObj.protocol)<0) return false

  // 域名部分验证
  if( !urlObj.hostname || (
      !etools.isDomain(urlObj.hostname) &&
      !etools.isIpv4(urlObj.hostname)   &&
      !etools.isIpv6(urlObj.hostname)   ))
    return false;

  // 验证端口
  try{
    const port=Number.parseInt(urlObj.port);
    if(port===0 || port>65535) return false
  } catch (e) {
    return false
  }

  // 至此认为验证成功
  return true
};
// ajax请求
/**
 * [ajaxObj 定义]
 *
 * url             ×请求的地址，含协议、路径
 * params           请求的附加值，默认为空
 * method           请求方式：GET，POST，默认根据是否有post来确定
 * reqMime          post数据体格式，默认json
 * post             POST方式时传递的对象，也可直接给raw
 * headers          头信息
 * forceResRawType  强制使用某种格式识别res，默认未定义
 * timeout          超时时间，秒
 */
etools.ajax=function(ajaxObj,ajaxRaw,callback){
  // 顺序调整
  if(typeof ajaxRaw==='function') {
    callback=ajaxRaw;
    ajaxRaw=undefined;
  }

  const headersObj={}; // 头信息

  // 验证callback是function
  if(!(typeof callback==='function')){
    throw new Error('etools.ajax must have Callback Function');
  }

  // 验证ajaxObj是Object
  if(!etools.typeMatch(ajaxObj,'object')){
    callback('ajaxObj is not Object');
    return
  }

  // 验证url
  if(!etools.isUrl(ajaxObj.url)){
    callback('ajaxObj.url is not url');
    return
  }


  /* ========== get附加信息，即ajaxObj.params，写进url  ========== */
  if(etools.typeMatch(ajaxObj.params,'object')){
    // 遍历ajaxObj.params
    for(const i in ajaxObj.params){
      const itype=typeof ajaxObj.params[i];
      const fchar=ajaxObj.url.indexOf('?')>=0?'&':'?';
      switch (itype){
        case 'string':
          ajaxObj.url+=fchar+i+'='+encodeURIComponent(ajaxObj.params[i]);
          break;
        case 'boolean':
          ajaxObj.url+=fchar+i+'='+ajaxObj.params[i]?'true':'false';
          break;
        case 'number':
          ajaxObj.url+=fchar+i+'='+ajaxObj.params[i];
          break;
        default:
          throw new Error('ajaxObj.params\'s item must be string boolen number');
          break
      }
    }
  }

  /* ========== method 项检测，支持 POST GET DELETE  ========== */

  // 未定义method，根据是否有post自动识别
  if(!ajaxObj.method){
    ajaxObj.method=(ajaxObj.post||ajaxRaw)?'POST':'GET'
  }
  // 有则检验
  else {
    //有method，验证
    if(typeof ajaxObj.method!=='string'){
      callback('ajaxObj.method is not String');
      return
    }
    ajaxObj.method=ajaxObj.method.toUpperCase();

    if(!['GET','PUT','DELETE'].includes(ajaxObj.method)){
      callback('ajaxObj.method is not POST/GET/DELETE');
      return
    }
  }

  /* ========== POST方式时，sendRaw内容生成 ========== */
  let sendRaw
  if(ajaxObj.method==='POST'){
    // 默认json格式
    ajaxObj.reqMime=ajaxObj.reqMime||'/'

    // 按照格式转换
    let mime,headers;
    try{
      [mime,sendRaw,headers]=etools.jr2mime_catch(ajaxObj.reqMime,ajaxObj.post,ajaxRaw);
    } catch (e) {
      callback(e);
      return;
    }

    // 写入需要的headers
    for(const key in headers){
      headersObj[key]=headers[key];
    }
  }

  /* ========== 自定义头信息写入 ========== */
  if(etools.typeMatch(ajaxObj.headers,'object')){
    for(const key in ajaxObj.headers){
      headersObj[key]=ajaxObj.headers[key];
    }
  }

  // ============== 整理完毕，调用请求 ==============
  const url=URL.parse(ajaxObj.url); // 分解url

  // 协议分解
  let prot;
  switch(url.protocol){
    // 所有协议的冒号不能省略
    case 'http:':
      if(!url.port) url.port=80; // 默认80号端口
      prot=require('http');
      break;
    case 'https:':
      if(!url.port) url.port=443; // 默认443号端口
      prot=require('https');
      break;
    default :
      callback('unKnow protocol: '+url.protocol);
      return
  }
  // 请求对象构建
  const reqObj={
    hostname:url.hostname,
    port:url.port,
    path:url.path,
    method:ajaxObj.method,
    headers:headersObj
  };

  // 执行调用，接收返回结果
  const req=prot.request(reqObj,(res)=>{
    const json={
      statusCode:res.statusCode,
      headers:res.headers,
      mime:ajaxObj.resMime || res.headers['content-type'] || '/',
      cookie:res['set-cookie']
    };

    // 构建post部分接收体
    const rawArray=[];

    // content-length字段处理
    let errObj=null;
    try{
      const contentLength=Number.parseInt(res.headers['content-length']);
      if(contentLength>conf.ajaxReq.resMaxLength) errObj={statusCode:413,statusMessage:'Body too long'}
    }
    catch (e){
      // 获取长度失败
      if(!conf.ajaxReq.resAlowNoContentLength) errObj={statusCode:480,statusMessage:'resHeader do not have Content-Length'}
    }
    if(errObj){
      doResponse(req,res,errObj);
      return
    }

    let length=0;
    let tooLong=false;

    // 读取数据
    res.on('data', (data) => {
      // 如果已经超长，不再执行
      if(tooLong) return;

      // 累计长度，并验证是否超长
      length+=data.length;
      if(length>conf.ajaxReq.resMaxLength){
        doResponse(req,res,{statusCode:413,statusMessage:'Body too long'});
        res.client.destroy();
        tooLong=true;
        return
      }
      // 数据写入数组
      rawArray.push(data)
    });
    res.on('end', () => {
      // 如果之前已经因为超长报错，则不需要执行此处
      if(tooLong) return;

      // 合并完整的接收流
      const raw=Buffer.concat(rawArray);

      // post请求发过来的长度
      json.rawLength=raw.length;

      // 尝试将转出
      let err,resJson,resRaw;
      try{
        [resJson,resRaw]=etools.mime2jr_catch(json.mime,raw);
      } catch(e) {
        err=e
      }

      // 根据请求状态码是否是200，决定将resJson放在哪个位置
      if(res.statusCode===200) json.body=resJson;
      else err=resJson;

      // 执行响应
      doResponse(req,res,err,json,resRaw);


      return;
      // 根据res不同的mime选择处理
      switch (json.mime){
        case 'json':
          // bug 20171219 : try里面不能出现doResponse
          let err;
          try{
            json.body=JSON.parse(raw.toString('utf8'));
          }catch (e){err=e}
          // 是否有错误发生
          if(err){
            doResponse(req,res,{statusCode:481,statusMessage:'json body error'});
          } else {
            doResponse(req,res,undefined,json);
          }
          break;
        default:
          doResponse(req,res,undefined,json,raw);
          break;
      }
    });
  });

  // 超时时间
  const timeout=ajaxObj.timeout || 15;
  // 如果到达超时事件，仍未反馈，则调用callback
  let timeClock=setTimeout(()=>{
    callback('timeout');
    callback=()=>{};// 超时之后得到响应的回调函数
  },timeout*1000);

  req.on('error', (e) => {
    clearTimeout(timeClock);
    callback(e);
  });

  // 如果有发送数据体，则执行发送
  if(ajaxObj.method==='POST') req.write(sendRaw);
  req.end();

  // 如果有回调函数，则处理接收到的反馈
  function doResponse(req,res,err,json,raw){
    if(typeof callback==='function'){
      clearTimeout(timeClock);
      callback(err,json,raw);
    }
  }
};

// ========== 格式验证类，返回true/false =============

// 字符串正则表达式匹配，非字符串必定为false
function _strTestRegex(str,regex){
  if(typeof str!=='string') return false  // 检验输入格式
  var re=new RegExp(regex)                // 生成正则对象
  return re.test(str)                     // 返回验证结果
}

// int型数字
etools.isInt=function(num){
  return Number.isSafeInteger(num);
};
// 是否是手机号
etools.isPhoneNum=function(num){
  // 若是字符串，则先转数字
  if(typeof num==='string') num=Number.parseFloat(num)

  // 反馈判断结果
  return etools.isInt(num)&&num>130e8&&num<200e8
};

// 类型匹配检验，拓展typeof
/*
* obj 为要验证的变量
* etype 为验证类型，若给数组，匹配其一即可
*
*
* jsType    etype
* ======================
* object    object
*           array
*           buffer
*           null
* ----------------------
* number    timestamp
*           int  (include timestamp)
*           float(include int)
*           NaN
*           +Infinity
*           -Infinity
*           Infinity(include + -)
*           number(include ALL)
* */
etools.typeMatch=function selfFunc(obj,typeStr){
  // 若typeStr是数组，则逐个类型尝试，符合一个则反馈true
  if(Array.isArray(typeStr)){
    // 只要数组中有一种类型匹配成功，就反馈true
    for(const i in typeStr){
      // 递归调用单类型判断
      if(selfFunc(obj,typeStr[i])) return true
    }
    // 均不成功，反馈false
    return false
  }

  // 单类型判断
  switch(typeof obj){
    case 'object':
      if(Array.isArray(obj)){
        return typeStr==='array'
      } else if(Buffer.isBuffer(obj)){
        return typeStr==='buffer'
      } else if(obj===null){
        return [null,'null'].includes(typeStr)
      } else {
        return typeStr==='object'
      }
      break;
    case 'number':
      // 仅仅判定number
      if(typeStr==='number') return true;
      // int
      else if(etools.isInt(obj)){
        if(['int','float'].includes(typeStr)){
          return true
        } else if(obj>=1e9&&obj<1e10 || obj>=1e12&&obj<1e13){
          return typeStr==='timestamp'
        }
        return false
      }
      // NaN
      else if(Number.isNaN(obj)){
        return typeStr==='NaN'
      }
      // +Infinity
      else if(obj===+Infinity){
        return ['Infinity','+Infinity'].includes(typeStr)
      }
      // -Infinity
      else if(obj===-Infinity){
        return ['Infinity','-Infinity'].includes(typeStr)
      }
      // float
      else {
        return typeStr==='float'
      }
      break;
    default:
      return (typeof obj===typeStr)
  }
};
// 返回原型类别
etools.protoType=function(obj){
  return Object.prototype.toString.call(obj);
};
// 返回east自定义类别
etools.etype=function(obj){
  switch (Object.prototype.toString.call(obj)){
    case '[object Object]':return 'dict';
    case '[object Array]':return 'array';
    case '[object Null]':return 'null';
    case '[object RegExp]':return 'regex';
    case '[object Uint8Array]':return 'buffer';
    case '[object Undefined]':return 'undefined';
    case '[object Number]':return 'number';
    case '[object String]':return 'string';
    case '[object Function]':return 'function';
    default:return 'unknow';
  }
}
/* =========== 加密解密相关 =========== */

// md5加密
etools.md5=function(data,inEncoding,outEncoding){
  const md5 = crypto.createHash('md5');
  md5.update(data,inEncoding);
  return md5.digest(outEncoding);
};

// 产生指定长度的随机字符串
etools.ranStr=(len)=>{
  var $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
  var maxPos = $chars.length;
  var pwd = '';
  for (var i = 0; i < len; i++) {
    pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
  }
  return pwd;
};

/* =========== 等待移出的方法 =========== */
etools.tagkey=function(str){
  let err=null,cid=null,tagid=null;
  if(typeof str!=='string'){
    err='str must be String'
  } else {
    // 确定str是字符串
    const arry=str.split('_');
    if(arry.length!==2){
      err='str must split by _ to 2 part'
    } else {
      // 确定可分成两部分
      tagid=Number.parseFloat(arry[1]);
      if(!etools.isInt(tagid)){
        err='tagid must be INT'
      } else {
        // 确定tagid是INT
        cid=arry[0]
      }
    }
  }
  return [err,cid,tagid]
};

etools.hostname=os.hostname();

etools.SUCCESS={status:'ok'};

module.exports=etools;